////  Fears.swift
//  SwiftUIListFilterWithStateAndToggle
//
//  Created on 15/11/2020.
//  
//

import Foundation

struct Fear: Identifiable {
    
    let id = UUID()
    let name: String
    let image: String
    let isFear: Bool
    
}

extension Fear {
    
    static func addAll() -> [Fear] {
        return [
            Fear(name: "Fire", image: "1", isFear: true),
            Fear(name: "Skull", image: "2", isFear: true),
            Fear(name: "Leaf", image: "3", isFear: false)
        ]
    }
    
}
