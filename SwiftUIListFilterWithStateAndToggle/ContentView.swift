////  ContentView.swift
//  SwiftUIListFilterWithStateAndToggle
//
//  Created on 15/11/2020.
//  
//

import SwiftUI

struct ContentView: View {
    
    let fears = Fear.addAll()
    
    @State var isFear: Bool = false
    
    
    var body: some View {
        
        
        Toggle(isOn: $isFear ) {
            Text("Fear")
                .padding(.leading)
        }
        
        
        List {
            
            ForEach(fears.filter { $0.isFear == self.isFear }) { fear in
                
                HStack {
                    Image(fear.image)
                        .resizable()
                        .frame(width: 100, height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    
                    Text(fear.name)
                        .font(.title)
                    
                    Spacer()
                    
                    if (fear.isFear)
                    {
                        Image("6")
                            .resizable()
                            .frame(width: 50, height: 50, alignment: .trailing)
                    }
                }
            }
        }
    
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
